package ginseng.domain;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import ginseng.dao.AbstractPlayerRepository;
import ginseng.dao.BoardRepository;
import ginseng.dao.MatchRepository;
import ginseng.dao.MovementRepository;
import ginseng.dao.TokenRepository;
import ginseng.domain.kuar.KuarGame;
import ginseng.domain.ppt.PPTGame;
import ginseng.domain.tictactoe.TictactoeGame;
//import ginseng.websocket.noSpring.WSServer;

@Component
public class Manager {
	@Autowired
	private AbstractPlayerRepository playersRepo;
	@Autowired 
	private TokenRepository tokensRepo;
	@Autowired
	private MatchRepository matchesRepo;
	@Autowired
	private BoardRepository boardsRepo;
	@Autowired
	private MovementRepository movementsRepo;
	
	private ConcurrentHashMap<Integer, Game> games;
	private ConcurrentHashMap<String, AbstractPlayer> players;
	protected ConcurrentHashMap<String, Match> inPlayMatches;
	
	public Manager() {
		this.inPlayMatches=new ConcurrentHashMap<>();
		games=new ConcurrentHashMap<>();
		KuarGame s3x3=new KuarGame();
		s3x3.setRows(3);
		games.put(1, s3x3);
		KuarGame s4x4=new KuarGame();
		s4x4.setRows(4);
		games.put(2, s4x4);
		KuarGame s5x5=new KuarGame();
		s5x5.setRows(5);
		games.put(3, s5x5);

		Game tictactoe=new TictactoeGame();
		games.put(10, tictactoe);
		Game ppt=new PPTGame();
		games.put(11, ppt);

		this.players=new ConcurrentHashMap<>();
	}
	
	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	@Bean
	public static Manager get() {
		return ManagerHolder.singleton;
	}
	
	public Match joinGame(AbstractPlayer player, int idGame) throws Exception {
		if (this.players.get(player.getUserName())==null)
			throw new Exception("User not logged");
		Game game=this.games.get(idGame);
		Match match = game.getMatch(player);
		if (match.isComplete()) {
			matchesRepo.save(match);
			game.pendingMatches.remove(match.getIdMatch());
		}
		return match;
	}
	
	public Match joinGame(AbstractPlayer player, String gameName) throws Exception {
		Enumeration<Game> games=this.games.elements();
		while (games.hasMoreElements()) {
			Game game=games.nextElement();
			if (game.getName().equals(gameName)) {
				Match match = game.getMatch(player);
				if (match.isComplete()) {
					game.pendingMatches.remove(match.getIdMatch());
					matchesRepo.save(match);
					//WSServer.startMatch(match);
				}
				return match;
			}
		}
		return null;
	}

	public Match move(String idMatch, AbstractPlayer player, JSONArray coordinates) throws Exception {
		Integer[] iC=new Integer[coordinates.length()];
		for (int i=0; i<iC.length; i++)
			iC[i]=coordinates.getInt(i);
		Match match=this.inPlayMatches.get(idMatch);
		if (match==null)
			throw new Exception("La partida ha terminado");
		Movement movement=match.premove(player, iC);
		movementsRepo.save(movement);
		match.postmove(player);
		if (match.getWinner()!=null) {
			match.setFinished(true);
			matchesRepo.save(match);
		}			
		return match;
	}
	
	public JSONObject logout(AbstractPlayer player) throws Exception {
		JSONObject jso=new JSONObject();
		this.userLeaves(player.getUserName());
		return jso;
	}
	
	public AbstractPlayer login(String userName, String pwd) throws Exception {
		if (userName.length()==0 || pwd.length()==0)
			throw new Exception("Credenciales inválidas");
		
		AbstractPlayer player=playersRepo.findByUserName(userName);
		if (player==null || !((Player) player).getPwd().equals(pwd))
			throw new Exception("Credenciales inválidas");
		this.players.put(userName, player);
		return player;
	}

	public AbstractPlayer login(String userName) throws Exception {
		AbstractPlayer player=playersRepo.findByUserName(userName);
		if (player==null)
			throw new Exception("Credenciales inválidas");
		this.players.put(userName, player);
		return player;
	}
	
	public AbstractPlayer register(String email, String userName, String pwd1, String pwd2) throws Exception {
		if (!pwd1.equals(pwd2))
			throw new Exception("Error: las contraseñas no coinciden");
		Player player=new Player();
		player.setEmail(email);
		player.setUserName(userName);
		player.setPwd(pwd1);
		
		AbstractPlayer insertedPlayer=playersRepo.save(player);
		return insertedPlayer;
	}
	

	public Token requestToken(String userName) throws Exception {
		Token token=new Token();
		token.setUserName(userName);
		tokensRepo.save(token);
		return token;
	}

	public void resetPwd(String userName, String pwd, String idToken) throws Exception {
		Token token=tokensRepo.findById(idToken).get();
		if (token==null || !token.getUserName().equals(userName) || token.getValidTime()<System.currentTimeMillis())
			throw new Exception("Token expirado o token no existe");
		Player player=(Player) playersRepo.findByUserName(userName);
		player.setPwd(pwd);
		playersRepo.save(player);
	}

	public AbstractPlayer simpleRegister(String userName) throws Exception {
		SimplePlayer player=new SimplePlayer();
		player.setUserName(userName);
		AbstractPlayer insertedPlayer=playersRepo.save(player);
		return insertedPlayer;
	}
	
	public void addInPlayMatch(Match match) {
		inPlayMatches.put(match.getIdMatch(), match);
	}

	public void removeInPlayMatch(String id) {
		inPlayMatches.remove(id);
	}

	public ConcurrentHashMap<String, Match> getInPlayMatches() {
		return inPlayMatches;
	}
	
	public AbstractPlayer leaveMatch(String idMatch, String userName) throws Exception {
		Match match=getInPlayMatches().get(idMatch);
		removeInPlayMatch(idMatch);
		
		AbstractPlayer winner, looser;
		if (match.getPlayerA().getUserName().equals(userName)) {
			winner=match.getPlayerB();
			looser=match.getPlayerA();
		} else {
			winner=match.getPlayerA();
			looser=match.getPlayerB();
		}
		winner.increaseVictoriesDefeatsWithdrawals(1, 0, 0);
		looser.increaseVictoriesDefeatsWithdrawals(0, 1, 1);		
		playersRepo.save(winner);
		playersRepo.save(looser);
		match.setFinished(true);
		match.setWinner(winner);
		matchesRepo.save(match);
		winner.setCurrentMatch(null);
		looser.setCurrentMatch(null);
		return winner;
	}
	
	public void userLeaves(String userName) throws Exception {
		AbstractPlayer player = this.players.remove(userName);
		if (player.getCurrentMatch()!=null)
			this.leaveMatch(player.getCurrentMatch().getIdMatch(), userName);
	}

	public Game findGame(int idGame) {
		return this.games.get(idGame);
	}
}
