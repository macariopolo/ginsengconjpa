package ginseng.domain.kuar;

import ginseng.domain.Board;
import ginseng.domain.Game;
import ginseng.domain.Match;

public class KuarGame extends Game {
	private int rows;
	
	public KuarGame() {
	}

	@Override
	public String getName() {
		return "Kuar " + rows + "x" + rows;
	}

	@Override
	protected Match createMatch() {
		Board board=getRandomBoard(true);
		Match match = new KuarMatch();
		match.setBoard(board);
		return match;
	}

	private Board getRandomBoard(boolean testingMode) {
		String id="5c20a4ec4034f8ea0c53dc96";		
	    String contenido="0,0,5,7,4,1,3,6,2";
	    
	    KuarBoard board=new KuarBoard(id, 3, contenido);
		return board;
	}

	public void setRows(int rows) {
		this.rows=rows;
	}
}
