package ginseng.domain.kuar;

import org.json.JSONObject;

import ginseng.domain.AbstractPlayer;
import ginseng.domain.Board;
import ginseng.domain.Match;

public class KuarBoard extends Board {
	private int rows;
	private Integer[][] squares;
	private int max;
	
	public KuarBoard() {
		super();
		this.squares=new Integer[rows][rows];
	}

	public KuarBoard(Match match) {
		super(match);
	}

	public KuarBoard(String idBoard, int rows, String content) {
		this(null);
		this._id=idBoard;
		this.rows=rows;
		this.squares=new Integer[rows][rows];
		setContent(content);
	}

	@Override
	protected void move(AbstractPlayer player, Integer[] coordinates) throws Exception {
		KuarBoard board;
		KuarMatch match=(KuarMatch) this.match;
		if (player==this.match.getPlayerA())
			board=match.getBoardA();
		else
			board=match.getBoardB();
		boolean completed=doMovement(board, coordinates);
		if (completed)
			this.match.setWinner(player);
	}
	
	private boolean doMovement(KuarBoard board, Integer[] coordinates) throws Exception {
		int rowOrigin=coordinates[0];
		int colOrigin=coordinates[1];
		int rowDst=coordinates[2];
		int colDst=coordinates[3];
		int n=board.squares[rowOrigin][colOrigin];
		board.squares[rowOrigin][colOrigin]=0;
		board.squares[rowDst][colDst]=n;
		return board.isCompleted();
	}

	@Override
	public AbstractPlayer getWinner() {
		return this.match.getWinner();
	}
	
	protected boolean isCompleted() {
		Integer[] start=new Integer[2];
		boolean doBreak=false;
		for (int i=0; i<rows; i++) {
			for (int j=0; j<rows; j++)
				if (squares[i][j]==1) {
					start[0]=i; 
					start[1]=j;
					doBreak=true;
					break;
				}
			if (doBreak)
				break;
		}
		do {
			start=findAdjacent(start);
		} while (start!=null && squares[start[0]][start[1]]!=max);
		return start!=null;
	}

	private Integer[] findAdjacent(Integer[] start) {
		int row=start[0];
		int col=start[1];
		int arriba=row-1;
		int abajo=row+1;
		int izda=col-1;
		int dcha=col+1;
		if (arriba>=0 && this.squares[arriba][col]==this.squares[row][col]+1)
			return new Integer[] { arriba, col };
		if (abajo<=2 && this.squares[abajo][col]==this.squares[row][col]+1)
			return new Integer[] { abajo, col };
		if (izda>=0 && this.squares[row][izda]==this.squares[row][col]+1)
			return new Integer[] { row, izda };
		if (dcha<=2 && this.squares[row][dcha]==this.squares[row][col]+1)
			return new Integer[] { row, dcha };
		return null;
	}

	@Override
	public boolean end() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getContent() {
		String r="";
		for (int i=0; i<rows; i++)
			for (int j=0; j<rows; j++)
				r = r + this.squares[i][j] + ",";
		r=r.substring(0, r.length()-1);
		return r;
	}

	@Override
	public JSONObject toJSON() throws Exception {
		JSONObject jso=new JSONObject();
		jso.put("idBoard", this._id.toString()).put("type", this.getClass().getSimpleName()).put("rows", this.rows).put("content", this.getContent());
		return jso;
	}

	public void setContent(String content) {
		int contador=0;
		String[] tokens=content.split(",");
		for (int i=0; i<rows; i++) 
			for (int j=0; j<rows; j++) {
				this.squares[i][j]=Integer.parseInt(tokens[contador++]);
				if (this.squares[i][j]>max)
					max=this.squares[i][j];
			}
	}
	
	public void setRows(int rows) {
		this.rows = rows;
		this.squares=new Integer[rows][rows];
	}

	public int getRows() {
		return this.rows;
	}
}
