package ginseng.domain;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;

@Component
@Entity
public class Player extends AbstractPlayer {
	private String email;
	private String pwd;
	
	public Player() {
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPwd() {
		return pwd;
	}
	
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
