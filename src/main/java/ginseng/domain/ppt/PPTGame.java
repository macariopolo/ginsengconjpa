package ginseng.domain.ppt;

import ginseng.domain.Game;
import ginseng.domain.Match;

public class PPTGame extends Game {
	
	public PPTGame() {
		super();
	}

	@Override
	public String getName() {
		return "PPT";
	}

	@Override
	protected Match createMatch() {
		return new PPTMatch();
	}

}
