package ginseng.domain;

import java.util.Random;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="token")
public class Token {
	@Id 
	private String id;
	@Column(name = "valid_time")
	private Long validTime;
	@Column(name = "user_name")
	private String userName; 
	
	public Token() {
		this.id=UUID.randomUUID().toString();
		this.validTime=System.currentTimeMillis() + 10*60*1000;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}
	
	public long getValidTime() {
		return validTime;
	}
}
