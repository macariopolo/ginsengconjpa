package ginseng.domain;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ginseng.dao.MovementRepository;

@Entity(name="matchs")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="game")
public abstract class Match {
	@Autowired
	@Transient
	private MovementRepository movementsRepo;
	
	@Id
	@Column(name="id")
	protected String idMatch;
	@ManyToOne
	@JoinColumn(name="player_a")
	protected AbstractPlayer playerA;
	@ManyToOne
	@JoinColumn(name="player_b")
	protected AbstractPlayer playerB;
	@Transient
	protected Integer currentPlayer;
	@ManyToOne
	@JoinColumn(name="winner")
	protected AbstractPlayer winner;
	//@ManyToOne
	//@JoinColumn(name="id_board")
	@Transient
	protected Board board;
	@Transient
	private Game game;
	@SuppressWarnings("unused")
	private boolean finished;
	
	public Match() {
		this.idMatch=UUID.randomUUID().toString();
		this.currentPlayer=-1;
	}

	public void setGame(Game game) {
		this.game=game;
	}

	public String getIdMatch() {
		return idMatch;
	}

	public void addPlayer(AbstractPlayer player) {
		if (this.playerA==null)
			this.playerA=player;
		else 
			this.playerB=player;
		player.setCurrentMatch(this);
	}
	
	public AbstractPlayer getPlayerA() {
		return playerA;
	}

	public AbstractPlayer getPlayerB() {
		return playerB;
	}

	public Board getBoard() {
		return board;
	}
	
	public AbstractPlayer getWinner() {
		return winner;
	}
	
	public String getWinnerName() {
		return winner==null ? null : winner.getUserName();
	}
	
	public int getCurrentPlayer() {
		return currentPlayer;
	}
	
	public String getCurrentPlayerUserName() {
		if (!isComplete())
			return null;
		return currentPlayer==0 ? this.playerA.getUserName() : this.playerB.getUserName();
	}
	
	public Game getGame() {
		return game;
	}
	
	public String getGameName() {
		return game.getName();
	}
	
	public Movement premove(AbstractPlayer player, Integer[] coordinates) throws Exception {
		if (!tieneElTurno(player))
			throw new Exception("No tienes el turno");
		this.board.move(player, coordinates);
		Movement movement=new Movement(this, player, coordinates);
		return movement;
	}
	
	public Match postmove(AbstractPlayer player) throws Exception {
		this.winner=this.board.getWinner();
		if (winner!=null) {
			this.playerA.setCurrentMatch(null);
			this.playerB.setCurrentMatch(null);
			this.finished=true;
			Manager.get().removeInPlayMatch(this.getIdMatch());
		}
		this.currentPlayer=(this.currentPlayer+1) % 2;
		return this;
	}
	
	protected abstract boolean tieneElTurno(AbstractPlayer player);

	public abstract void calculateFirstPlayer();

	public void setWinner(AbstractPlayer player) {
		this.winner=player;
	}

	public boolean isComplete() {
		return this.playerA!=null && this.playerB!=null;
	}

	public void setBoard(Board board) {
		this.board=board;
		board.setMatch(this);
	}

	public boolean isFinished() {
		return this.finished;
	}

	public void setFinished(boolean finished) {
		this.finished=finished;
	}
}
