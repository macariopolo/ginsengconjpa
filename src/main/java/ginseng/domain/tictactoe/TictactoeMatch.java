package ginseng.domain.tictactoe;

import java.util.Random;

import javax.persistence.Entity;

import ginseng.domain.AbstractPlayer;
import ginseng.domain.Match;

@Entity
public class TictactoeMatch extends Match {
	public TictactoeMatch() {
		super();
		this.board=new TictactoeBoard(this);
	}

	@Override
	public void calculateFirstPlayer() {
		boolean dado=new Random().nextBoolean();
		this.currentPlayer=dado ? 0 : 1;
		this.currentPlayer=0;   // puesto a propósito con fines de desarrollo y de test para que empiece el primer jugador
	}

	@Override
	protected boolean tieneElTurno(AbstractPlayer player) {
		return (this.getCurrentPlayer()==0 && player==this.playerA) || (this.getCurrentPlayer()==1 && player==playerB);
	}
}
