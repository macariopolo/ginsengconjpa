package ginseng.domain.tictactoe;

import ginseng.domain.Game;
import ginseng.domain.Match;

public class TictactoeGame extends Game {

	public TictactoeGame() {
		super();
	}

	@Override
	public String getName() {
		return "tictactoe";
	}

	@Override
	protected Match createMatch() {
		Match match=new TictactoeMatch();
		match.setGame(this);
		return match;
	}

}
