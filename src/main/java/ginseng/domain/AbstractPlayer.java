package ginseng.domain;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Transient;

@Entity(name="abstractplayer")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="player_type")
public abstract class AbstractPlayer {
	@Id
	protected String userName;
	@Transient
	protected Match currentMatch;
	protected int victories;
	protected int defeats;
	protected int withdrawals;
	
	public AbstractPlayer() {}
		
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setCurrentMatch(Match match) {
		this.currentMatch=match;
	}
	
	public Match getCurrentMatch() {
		return currentMatch;
	}
	
	public void increaseVictoriesDefeatsWithdrawals(int victories, int defeats, int withdrawals) throws Exception {
		this.victories+=victories;
		this.defeats+=defeats;
		this.withdrawals+=withdrawals;
	}

	public int getVictories() {
		return victories;
	}
	
	public int getDefeats() {
		return defeats;
	}
	
	public int getWithdrawals() {
		return withdrawals;
	}
}
