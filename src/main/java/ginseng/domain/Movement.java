package ginseng.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

@Entity(name="movement")
public class Movement {
	@Id @GeneratedValue(strategy= GenerationType.AUTO, generator="native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name="id")
	private Long id;
	@ManyToOne @JoinColumn(name="id_match")
	private Match match;
	@ManyToOne @JoinColumn(name="player")
	private AbstractPlayer player;
	@SuppressWarnings("unused")
	private String coordinates;
	
	public Movement(Match match, AbstractPlayer player, Integer[] coordinates) {
		this.match=match;
		this.player=player;
		this.coordinates="";
		for (int i=0; i<coordinates.length-1; i++)
			this.coordinates+=coordinates[i]+",";
		this.coordinates+=coordinates[coordinates.length-1];
	}
}
