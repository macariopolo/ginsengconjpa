package ginseng.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ginseng.domain.Token;

@Repository
public interface TokenRepository extends CrudRepository<Token, String>{

}
