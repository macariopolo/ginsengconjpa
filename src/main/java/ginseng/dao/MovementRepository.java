package ginseng.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ginseng.domain.Movement;

@Repository
public interface MovementRepository extends CrudRepository<Movement, Long>{

	@Query("select count(*) from movement where id_match=:idMatch")
	Long findMovementsOfMatch(@Param("idMatch") String idMatch);
}
