package ginseng.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ginseng.domain.Match;

@Repository
public interface MatchRepository extends CrudRepository<Match, String>{

}
