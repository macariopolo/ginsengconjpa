package ginseng.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ginseng.domain.Board;

@Repository
public interface BoardRepository extends CrudRepository<Board, String>{

}
