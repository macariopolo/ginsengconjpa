package ginseng.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ginseng.domain.AbstractPlayer;

@Repository
public interface AbstractPlayerRepository extends CrudRepository<AbstractPlayer, String>{
	AbstractPlayer findByUserName(String userName);
	
	@Transactional
	@Modifying
	@Query(value= "update abstractplayer set pwd=? where user_name=?", nativeQuery=true)
	void updatePwd(String user_name, String pwd);
	
	@Transactional
	@Modifying
	@Query("delete from abstractplayer")
	void deleteAll();
}
