package ginseng;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Enumeration;

import org.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import ginseng.dao.AbstractPlayerRepository;
import ginseng.dao.MatchRepository;
import ginseng.dao.MovementRepository;
import ginseng.domain.AbstractPlayer;
import ginseng.domain.Game;
import ginseng.domain.Manager;
import ginseng.domain.Match;
import ginseng.domain.Token;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TodosLosTest {
	@Autowired
	protected AbstractPlayerRepository playersRepo;
	@Autowired
	protected MatchRepository matchesRepo;
	@Autowired
	protected MovementRepository movementsRepo;
	
	@Test
	public void execute() {
		String s="La ejecución va a comenzar en ";
		for (int i=5; i>=0; i--) {
			System.out.println(s + i + " segundos");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}

		for (int i=0; i<1; i++) {
			long timeIni=System.currentTimeMillis();
			setUp(); testKuar3x3LastSimple();
			setUp(); testKuar3x3LastClassic();
			setUp(); testLeavesPlayerA();
			setUp(); testLeavesPlayerB();
			setUp(); testManyMatchesKuar();
			setUp(); testRecoverPwd();
			setUp(); testRecoverPwdInvalidToken();
			setUp(); testJoinGameWithNoLogin();
			setUp(); testLoginAndLogout();
			setUp(); testTicTacToe();
			setUp(); testManyMatches();
			setUp(); testWrongPassword();
			long timeFin=System.currentTimeMillis();
			System.out.println("Time: " + (timeFin-timeIni) + " milliseconds");
		}
	}
	
	public void setUp() {
		playersRepo.deleteAll();
	}
	
	public void testKuar3x3LastSimple() {
		AbstractPlayer john=null, ann=null;
		try {
			john = Manager.get().register("john@john.com", "john", "john123", "john123456");
			fail();
		} catch (Exception e) {
		}
		try {
			john = Manager.get().register("john@john.com", "john", "john123", "john123");
			ann=Manager.get().simpleRegister("ann");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		Match match=null;
		try {
			Manager.get().login("ann");
			Manager.get().login("john", "john123");
			Match johnMatch = Manager.get().joinGame(john, 1);
			Match annMatch = Manager.get().joinGame(ann, "Kuar 3x3");
			
			assertSame(johnMatch, annMatch);
			
			match=johnMatch;
			
			Match readedMatch=matchesRepo.findById(match.getIdMatch()).get();
			assertTrue(readedMatch.getPlayerA().getUserName().equals(john.getUserName()));
			assertTrue(readedMatch.getPlayerB().getUserName().equals(ann.getUserName()));
			
			JSONArray coordinates=new JSONArray().put(0).put(2).put(0).put(1);  // 5 a la izda
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 1));
			
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 2));
			
			coordinates=new JSONArray().put(0).put(1).put(0).put(2);		// 5 a la dcha
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 3));
			
			coordinates=new JSONArray().put(1).put(0).put(0).put(0);		// 7 arriba
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 4));
			
			coordinates=new JSONArray().put(1).put(1).put(1).put(0);		// 4 a la izda
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 5));
			
			coordinates=new JSONArray().put(2).put(1).put(1).put(1);	// 6 arriba
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 6));
			
			coordinates=new JSONArray().put(1).put(1).put(0).put(1);	// 6 arriba
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 7));
			
			coordinates=new JSONArray().put(1).put(2).put(1).put(1);	// 1 izda
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 8));
			
			coordinates=new JSONArray().put(1).put(1).put(2).put(1);	// 1 abajo
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 9));
			
			coordinates=new JSONArray().put(1).put(0).put(1).put(1);	// 4 dcha 
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 10));
			
			coordinates=new JSONArray().put(1).put(1).put(1).put(2);	// 4 dcha
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 11));
			
			coordinates=new JSONArray().put(2).put(0).put(1).put(0);	// 3 arriba
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 12));
			
			coordinates=new JSONArray().put(1).put(0).put(1).put(1);	// 3 dcha
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 13));
			
			coordinates=new JSONArray().put(2).put(1).put(2).put(0);	// 1 izda
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 14));
			
			coordinates=new JSONArray().put(2).put(2).put(2).put(1);	// 2 izda
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 15));
			
			assertTrue(isFinished(match, "john"));
		} catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
		

		JSONArray coordinates = new JSONArray().put(2).put(2).put(2).put(1);
		try {
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			fail("Expected exception");
		} catch (Exception e) {
		}
	}
	
	public void testKuar3x3LastClassic() {
		AbstractPlayer hector=null, nomeacuerdo=null;
		try {
			hector = Manager.get().register("hector@hector.com", "hector", "hector123", "hector123");
			nomeacuerdo=Manager.get().simpleRegister("nomeacuerdo");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			Manager.get().login("hector", "hector123");
			Manager.get().login("nomeacuerdo");
			Match nomeacuerdoMatch = Manager.get().joinGame(nomeacuerdo, "Kuar 3x3");
			Match hectorMatch = Manager.get().joinGame(hector, 1);
			assertSame(hectorMatch, nomeacuerdoMatch);

			Manager.get().leaveMatch(nomeacuerdoMatch.getIdMatch(), "nomeacuerdo");
		} catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
	}
	
	public void testLeavesPlayerA() {
		AbstractPlayer louise=null, judith=null;
		try {
			louise = Manager.get().register("louise@louise.com", "louise", "louise123", "louise123");
			judith=Manager.get().simpleRegister("judith");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			judith=Manager.get().login("judith");
			louise=Manager.get().login("louise", "louise123");
			Match louiseMatch = Manager.get().joinGame(louise, 1);
			Match judithMatch = Manager.get().joinGame(judith, "Kuar 3x3");
			
			assertSame(louiseMatch, judithMatch);
			
			Manager.get().logout(judith);
			
			assertTrue(isFinished(louiseMatch, "louise"));
		} catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
	}

	public void testLeavesPlayerB() {
		AbstractPlayer louise=null, judith=null;
		try {
			louise = Manager.get().register("luoise@john.com", "louise", "louise123", "louise123");
			judith=Manager.get().simpleRegister("judith");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			judith=Manager.get().login("judith");
			louise=Manager.get().login("louise", "louise123");
			Match louiseMatch = Manager.get().joinGame(louise, 1);
			Match judithMatch = Manager.get().joinGame(judith, "Kuar 3x3");
			
			assertSame(louiseMatch, judithMatch);
			
			Manager.get().logout(louise);
			
			assertTrue(isFinished(louiseMatch, "judith"));
		} catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
	}
	
	public void testManyMatchesKuar() {
		ArrayList<AbstractPlayer> players=null;
		try {
			players = register(2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i=0; i<players.size(); i++)
			try {
				Manager.get().login(players.get(i).getUserName(), "john123");
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		Game game=Manager.get().findGame(1);
		for (int i=0; i<players.size(); i++) {
			try {
				Manager.get().joinGame(players.get(i), "Kuar 3x3");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (i%2==0)
				assertTrue("Se esperaba 1 partida pendiente", game.getPendingMatches().size()==1);
			else
				assertTrue("Se esperaban 0 partidas pendientes", game.getPendingMatches().size()==0);
		}
		
		Enumeration<Match> matches = Manager.get().getInPlayMatches().elements();
		ArrayList<Thread> hilos=new ArrayList<>();
		
		while (matches.hasMoreElements()) {
			Match match=matches.nextElement();
			HiloMatch hm=new HiloMatch(match);
			Thread t=new Thread(hm);
			hilos.add(t);
			t.start();
		}
		for (int i=0; i<hilos.size(); i++)
			try {
				hilos.get(i).join();
			} catch (InterruptedException e) {}
	}
	
	private class HiloMatch implements Runnable {

		private String idMatch;
		private AbstractPlayer playerA;
		private AbstractPlayer playerB;
		private Match match;

		public HiloMatch(Match match) {
			this.match=match;
			this.idMatch=match.getIdMatch();
			this.playerA=match.getPlayerA();
			this.playerB=match.getPlayerB();
		}

		@Override
		public void run() {
			try {
				JSONArray coordinates=new JSONArray().put(0).put(2).put(0).put(1); // 5 izda
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(0).put(1).put(0).put(2); // 5 dcha
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(0).put(0).put(0);  // 7 arriba
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(1).put(1).put(0);  // 4 izda
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(2).put(1).put(1).put(1);  // 6 arriba
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(1).put(0).put(1);  // 6 arriba
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(2).put(1).put(1);   // 1 izda
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(1).put(2).put(1); 	// 1 abajo
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(0).put(1).put(1); 	// 4 dcha
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(1).put(1).put(2); 	// 4 dcha
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(2).put(0).put(1).put(0); 	// 3 arriba
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(1).put(0).put(1).put(1); 	// 3 dcha
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(2).put(1).put(2).put(0); 	// 1 izda
				Manager.get().move(idMatch, playerA, coordinates);
				
				coordinates=new JSONArray().put(2).put(2).put(2).put(1); 	// 2 izda
				Manager.get().move(idMatch, playerA, coordinates);
				
				assertTrue(numberOfMovements(idMatch, 14));
				assertTrue(isFinished(match, playerA.getUserName()));
			} catch (Exception e) {
				fail("Error en hilo: idMatch: " + idMatch + "\n" + e.getMessage());
			}
			
		}
	}
	
	public void testRecoverPwd() {
		AbstractPlayer peter=null;
		try {
			peter = Manager.get().register("peter@peter.com", "peter", "peter123", "peter123");
			Manager.get().login("peter", "peter123");
			Manager.get().logout(peter);
			
			Token token = Manager.get().requestToken("peter");
			Manager.get().resetPwd("peter", "peter1234", token.getId());
			Manager.get().login("peter", "peter1234");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
	}
	
	public void testRecoverPwdInvalidToken() {
		AbstractPlayer peter=null;
		try {
			peter = Manager.get().register("peter@peter.com", "peter", "peter123", "peter123");
			Manager.get().login("peter", "peter123");
			Manager.get().logout(peter);
			
			Token token = Manager.get().requestToken("peter");
			Manager.get().resetPwd("peter", "peter1234", "tokenInventado");
			fail("Exception expected");
		}
		catch (Exception e) {
		}
	}
	
	public void testJoinGameWithNoLogin() {
		AbstractPlayer peter=null;
		try {
			peter = Manager.get().register("peter@peter.com", "peter", "peter123", "peter123");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			Manager.get().joinGame(peter, 1);
			fail("Expected exception");
		} catch (Exception e) {
		}
	}
	
	public void testLoginAndLogout() {
		AbstractPlayer peter=null;
		try {
			peter = Manager.get().register("peter@peter.com", "peter", "peter123", "peter123");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			Manager.get().login("peter", "peter123");
			Manager.get().logout(peter);
		} catch (Exception e) {
			fail("Unexpected exception");
		}
	}
	
	public void testTicTacToe() {
		AbstractPlayer john=null, ann=null;
		try {
			john = Manager.get().register("john@john.com", "john", "john123", "john123456");
			fail();
		} catch (Exception e) {
		}
		try {
			john = Manager.get().register("john@john.com", "john", "john123", "john123");
			ann=Manager.get().simpleRegister("ann");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
		
		try {
			Manager.get().login("ann");
			Manager.get().login("john", "john123");
			Match johnMatch = Manager.get().joinGame(john, 10);
			Match annMatch = Manager.get().joinGame(ann, "tictactoe");
			
			assertSame(johnMatch, annMatch);
			
			Match match=johnMatch;
			assertTrue(match.getCurrentPlayerUserName().equals(john.getUserName()));
			
			Match readedMatch=matchesRepo.findById(match.getIdMatch()).get();
			assertTrue(readedMatch.getPlayerA().getUserName().equals(john.getUserName()));
			assertTrue(readedMatch.getPlayerB().getUserName().equals(ann.getUserName()));
						
			JSONArray coordinates=new JSONArray().put(1).put(1);
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(match.getCurrentPlayerUserName().equals(ann.getUserName()));
			assertTrue(numberOfMovements(match.getIdMatch(), 1));
			
			try {
				Manager.get().move(match.getIdMatch(), john, coordinates);
				fail("Unexpected exception: John has not the turn");
			}
			catch (Exception e) {
			}
			
			try {
				coordinates=new JSONArray().put(1).put(1);
				Manager.get().move(match.getIdMatch(), ann, coordinates);
				fail("Unexpected exception: Ann has moved to a occupied square");
			}
			catch (Exception e) {
			}
			
			coordinates=new JSONArray().put(0).put(0);
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			assertTrue(match.getCurrentPlayerUserName().equals(john.getUserName()));
			assertTrue(numberOfMovements(match.getIdMatch(), 2));
			
			coordinates=new JSONArray().put(0).put(1);
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 3));
			
			coordinates=new JSONArray().put(2).put(1);
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 4));
			
			coordinates=new JSONArray().put(0).put(2);
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 5));
			
			coordinates=new JSONArray().put(1).put(0);
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 6));
			
			coordinates=new JSONArray().put(1).put(2);
			Manager.get().move(match.getIdMatch(), john, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 7));
			
			coordinates=new JSONArray().put(2).put(0);
			Manager.get().move(match.getIdMatch(), ann, coordinates);
			assertTrue(numberOfMovements(match.getIdMatch(), 8));
			
			assertTrue(isFinished(match, "ann"));
		} catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}
	}
	
	public void testManyMatches() {
		ArrayList<AbstractPlayer> players=null;
		try {
			players = register(50);
		} catch (Exception e) {
			fail("Error registrando players en testManyMatches línea 501");
		}
		for (int i=0; i<players.size(); i++)
			try {
				Manager.get().login(players.get(i).getUserName(), "john123");
			} catch (Exception e) {
				fail("Error logueando players en testManyMatches línea 505");
			}
		
		Game game=Manager.get().findGame(10);
		for (int i=0; i<players.size(); i++) {
			try {
				Manager.get().joinGame(players.get(i), "tictactoe");
			} catch (Exception e) {
				fail("Error uniendo players al juego en testManyMatches línea 513");
			}
			if (i%2==0)
				assertTrue(game.getPendingMatches().size()==1);
			else
				assertTrue(game.getPendingMatches().size()==0);
		}
		
		Enumeration<Match> matches = Manager.get().getInPlayMatches().elements();
		ArrayList<Thread> hilos=new ArrayList<>();
		
		while (matches.hasMoreElements()) {
			Match match=matches.nextElement();
			HiloMatchTictactoe hm=new HiloMatchTictactoe(match);
			Thread t=new Thread(hm);
			hilos.add(t);
			t.start();
		}
		for (int i=0; i<hilos.size(); i++)
			try {
				hilos.get(i).join();
			} catch (InterruptedException e) {}
	}
	
	private class HiloMatchTictactoe implements Runnable {

		private String idMatch;
		private AbstractPlayer playerA;
		private AbstractPlayer playerB;
		private Match match;

		public HiloMatchTictactoe(Match match) {
			this.match=match;
			this.idMatch=match.getIdMatch();
			this.playerA=match.getPlayerA();
			this.playerB=match.getPlayerB();
		}

		@Override
		public void run() {
			try {
				Manager.get().move(idMatch, playerA, new JSONArray().put(1).put(1));
				Manager.get().move(idMatch, playerB, new JSONArray().put(0).put(0));
				Manager.get().move(idMatch, playerA, new JSONArray().put(0).put(1));
				Manager.get().move(idMatch, playerB, new JSONArray().put(2).put(2));
				Manager.get().move(idMatch, playerA, new JSONArray().put(2).put(1));
				assertTrue(numberOfMovements(idMatch, 5));
				assertTrue(isFinished(match, playerA.getUserName()));
			} catch (Exception e) {
				fail("Error en hilo: idMatch: " + idMatch + "\n" + e.getMessage());
			}
			
		}
	}

	
	public void testWrongPassword() {
		AbstractPlayer peter=null;
		try {
			peter = Manager.get().register("peter@peter.com", "peter", "peter123", "peter123");
		}
		catch (Exception e) {
			fail("Unexpected: " + e.getMessage());
		}

		try {
			Manager.get().login("peter", "peter1234");
			fail("Expected exception");
		} catch (Exception e) {
		}
	}

	private ArrayList<AbstractPlayer> register(int n) throws Exception {
		ArrayList<AbstractPlayer> players=new ArrayList<>();
		for (int i=1; i<=n; i++) {
			AbstractPlayer player = Manager.get().register("john" + i + "@john.com", "john" + i, "john123", "john123");
			players.add(player);
		}
		return players;
	}
	
	protected boolean isFinished(Match match, String winner) {
		Match loadedMatch=matchesRepo.findById(match.getIdMatch()).get();
		return loadedMatch.getWinner().getUserName().equals(winner) && loadedMatch.isFinished();
	}

	protected boolean numberOfMovements(String idMatch, int expected) {
		long n=movementsRepo.findMovementsOfMatch(idMatch);
		return n==expected;
	}
}
